from django.db import models

# Create your models here.

class pediatra(models.Model):

    rut_pediatra = models.CharField(max_length=12,blank=True,null=False)
    nombre_pediatra = models.CharField(max_length=60,blank=True,null=True)
    especialidad_pediatra = models.CharField(max_length=20,blank = True,null= True)
    correo_pediatra = models.EmailField()

    def __str__(self):
        return(self.nombre)


