from django.apps import AppConfig


class PediatriaConfig(AppConfig):
    name = 'pediatria'
