from django import forms
from .models import pediatra


class RegModelForm(forms.ModelForm):
    class Meta:
        model = pediatra
        campos = ['rut_pediatra', 'nombre_pediatra', 'especialidad_pediatra', 'correo_pediatra']
        exclude = ()
        def clean_mail(self):
            email = self.cleaned_data.get('correo_pediatra')
            email_base, proveedor = email.split('@')
            dominio, extension = proveedor.split('.')
            if not extension == 'com':
                raise forms.ValidationError('Solo se aceptan correos con extension .com')
            return email

class ContactForm(forms.Form):
    nombre_pediatra = forms.CharField(max_length=50, required= True)
    asunto = forms.CharField(max_length=100, required=False)
    email = forms.CharField(max_length=100, required=False)
    contenido = forms.CharField(widget=forms.Textarea)

