from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from .models import pediatra
from .forms import RegModelForm
from .forms import ContactForm
from django.views.generic import ListView
# Create your views here.

def registro_pediatra(request):
        form = RegModelForm(request.POST or None)
        if request.user.is_authenticated():
            titulo = 'BIENVENIDO %s' % (request.user)
        if form.is_valid():
            form_data = form.cleaned_data
            rutP = form_data.get('rut_pediatra')
            nombreP = form_data.get('nombre_pediatra')
            especialidadP = form_data.get('especialidad_pediatra')
            correoP = form_data.get('correo_pediatra')
            objeto = pediatra.objects.create(rut_pediatra=rutP,nombre_pediatra = nombreP, especialidad_pediatra=especialidadP, correo_pediatra = correoP)
            titulo = 'Bienvenido al Registro de Pediatras.'
        contexto = {
            'el_titulo': titulo,
            'el_formulario': form,
        }
        return render(request, 'registro_pediatra.html',contexto)




def contacto(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        formulario_nombre = form.cleaned_data.get('Nombre_pediatra')
        formulario_email = form.cleaned_data.get('email')
        formulario_mensaje = form.cleaned_data.get('contenido')
        asunto = form.cleaned_data.get('asunto')
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, 'roberto.moore@virginiogomez.cl']
        email_mensaje = 'Enviado por %s - Correo: %s - Mensaje: %s' %(formulario_nombre,formulario_email,formulario_mensaje)
        send_mail(asunto,
                  email_mensaje,
                  email_from,
                  email_to,
                  fail_silently=False)


       # print(form.cleaned_data)
        #nombre = form.cleaned_data.get('nombre')
        #email = form.cleaned_data.get('email')
        #mensaje = form.cleaned_data.get('mensaje')
        #print(nombre, email, mensaje)

        #for key in form.cleaned_data:
         #   print(key)
         #   print(form.cleaned_data.get(key))

        #for key, value in form.cleaned_data.items():
            #print(key,value)
    contexto = {
        'el_contacto': form,
    }
    return render(request, 'enviar_correo.html', contexto)
