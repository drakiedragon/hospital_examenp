from django.db import models

# Create your models here.


class Ficha_clinica(models.Model):

    enfermedades_detectadas = models.CharField(max_length=200,blank=True,null=True)
    tratamiento = models.CharField(max_length=200, blank=True, null=True)
    fecha_diagnostico_enfermedad = models.DateField(auto_now_add=False,auto_now=False)
    fecha_termino_tratamiento = models.DateField(auto_now_add=False,auto_now=False)
    resultado_tratamiento = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return str(self.pk)

class Paciente(models.Model):
    nombre = models.CharField(max_length=30, blank=True, null=True)
    rut = models.CharField(max_length=12, blank=True, null=True, unique=True)
    fecha_nacimiento = models.DateField(auto_now_add=False, auto_now=False)
    fecha_ingreso_paciente = models.DateField(auto_now_add=False, auto_now=True)
    numero_ficha_paciente = models.OneToOneField(Ficha_clinica, primary_key=True)

    def __str__(self):
            return str(self.nombre)


