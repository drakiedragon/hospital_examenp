# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-11 23:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pacientes', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='paciente',
            old_name='ficha_paciente',
            new_name='n_ficha_paciente',
        ),
    ]
