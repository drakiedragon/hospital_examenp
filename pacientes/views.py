from django.shortcuts import render
from .models import Paciente, Ficha_clinica
from .forms import PacRegModelForm, FicRegModelForm
# Create your views here.


def registro_paciente(request):
        form = PacRegModelForm(request.POST or None)
        if request.user.is_authenticated():
            titulo = 'BIENVENIDO %s' % (request.user)
        if form.is_valid():
            form_data = form.cleaned_data
            nombre2 = form_data.get('nombre')
            rut2 = form_data.get('rut')
            fecha_nac2 = form_data.get('fecha_nacimiento')
            f_ingr_pac = form_data.get('fecha_ingreso_paciente')
            nume_ficha = form_data.get('numero_ficha_paciente')
            objeto = Paciente.objects.create(nombre=nombre2, rut=rut2, fecha_nacimiento=fecha_nac2, fecha_ingreso_paciente= f_ingr_pac, numero_ficha_paciente= nume_ficha)
            titulo = 'Bienvenido al Registro de Pacientes.'
        contexto = {
            'el_titulo': titulo,
            'el_formulario': form,
        }
        return render(request, 'registro_pacientes.html',contexto)


def registro_fichas(request):
    form = FicRegModelForm(request.POST or None)
    if request.user.is_authenticated():
        titulo = 'BIENVENIDO %s' % (request.user)
    if form.is_valid():
        form_data = form.cleaned_data
        enferD = form_data.get('enfermedades_detectadas')
        procedimiento = form_data.get('tratamiento')
        f_d_enf = form_data.get('fecha_diagnostico_enfermedad')
        f_t_trat = form_data.get('fecha_termino_tratamiento')
        resu_t = form_data.get('resultado_tratamiento')
        objeto = Ficha_clinica.objects.create(enfermedades_detectadas=enferD, tratamiento=procedimiento,
                                         fecha_diagnostico_enfermedad=f_d_enf, fecha_termino_tratamiento=f_t_trat,
                                         resultado_tratamiento=resu_t)
        titulo = 'Bienvenido al Registro de Ficha pacientes.'
    contexto = {
        'el_titulo': titulo,
        'el_formulario': form,
    }
    return render(request, 'registro_fichas.html', contexto)