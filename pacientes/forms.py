from django import forms
from .models import Paciente, Ficha_clinica


class PacRegModelForm(forms.ModelForm):
    class Meta:
        model = Paciente
        campos = ['nombre', 'rut', 'fecha_nacimiento', 'fecha_ingreso_paciente','n_ficha_paciente']
        exclude = ()

class FicRegModelForm(forms.ModelForm):
    class Meta:
        model = Ficha_clinica
        campos = ['enfermedades_detectadas','tratamiento', 'fecha_diagnostico_enfermedad','fecha_termino_tratamiento', 'resultado_tratamiento']
        exclude = ()